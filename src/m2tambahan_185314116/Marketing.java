package m2tambahan_185314116;

public class Marketing extends Pegawai implements TugasBelajar {

    private int jamKerja;
    private boolean status;
    public Marketing() {
    }

    public Marketing(String nama, String nip, int golongan, int jamKerja, boolean status) {
        this.status=status;
        this.nama = nama;
        this.nip = nip;
        this.golongan = golongan;
        this.jamKerja = jamKerja;
    }

    public int getJamKerja() {
        return jamKerja;
    }

    public void setJamKerja(int jamKerja) {
        this.jamKerja = jamKerja;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public double hitLembur() {
        double lembur;
        if (this.jamKerja > 8) {
            return lembur = (this.jamKerja - 8) * 50000;
        } else {
            return 0;
        }
    }

    @Override
    public double hitungGatot() {
        return hitGajiPokok() + hitLembur();
    }

    @Override
    public boolean isSelesai() {
        return status;
    }

}
