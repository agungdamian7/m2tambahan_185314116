package m2tambahan_185314116;

public class Manager extends Pegawai implements TugasBelajar {

    private int jumAnak, jamKerja, istri;
    private boolean status;

    public Manager() {
    }

    public Manager(String nama, String nip, int golongan, int jumAnak, int jamKerja, int istri, boolean status) {
        this.status = status;
        this.nama = nama;
        this.nip = nip;
        this.golongan = golongan;
        this.jumAnak = jumAnak;
        this.jamKerja = jamKerja;
        this.istri = istri;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getJumAnak() {
        return jumAnak;
    }

    public void setJumAnak(int jumAnak) {
        this.jumAnak = jumAnak;
    }

    public int getJamKerja() {
        return jamKerja;
    }

    public void setJamKerja(int jamKerja) {
        this.jamKerja = jamKerja;
    }

    public int getIstri() {
        return istri;
    }

    public void setIstri(int istri) {
        this.istri = istri;
    }

    public double hitTunjangan() {
        double tunjangan;
        if (this.jumAnak > 3) {
            tunjangan = 3 * 100000;
        } else {
            tunjangan = this.jumAnak * 100000;
        }
        return tunjangan;
    }

    public double hitLembur() {
        double lembur;
        if (this.jamKerja > 8) {
            return lembur = (this.jamKerja - 8) * 50000;
        } else {
            return 0;
        }
    }

    @Override
    public double hitungGatot() {
        return hitGajiPokok() + hitTunjangan() + hitLembur();
    }

    @Override
    public boolean isSelesai() {

        return status;
    }

}
