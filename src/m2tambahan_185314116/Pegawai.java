package m2tambahan_185314116;

public class Pegawai implements Pendapatan{
    protected String nip, nama;
    protected int golongan;
    protected double gajiTotal;

    public Pegawai() {
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getGolongan() {
        return golongan;
    }

    public void setGolongan(int golongan) {
        this.golongan = golongan;
    }

    public double getGajiTotal() {
        return gajiTotal;
    }

    public void setGajiTotal(double gajiTotal) {
        this.gajiTotal = gajiTotal;
    }
    
    public double hitGajiPokok(){
        
        switch (this.golongan) {
            case 1:
                return 500000;
            case 2:
                return 750000;
            default:
                return 1000000;
        }
    }

    @Override
    public double hitungGatot() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    
}
