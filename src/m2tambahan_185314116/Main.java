package m2tambahan_185314116;

import java.util.Scanner;

public class Main {

    static String nip, nama, statusS;
    static int golongan, jumAnak, jamKerja, istri, jumlah, anggotaM,
            anggotaH;
    static Kantor kantor = new Kantor();
    static Scanner baca = new Scanner(System.in);
    static boolean status, putar;

    static void rekamPegawai() {
        System.out.print("Nama\t\t\t: ");
        nama = baca.next();
        System.out.print("NIP\t\t\t: ");
        nip = baca.next();
        System.out.print("Golongan\t\t: ");
        golongan = baca.nextInt();
    }

    static void cetak(Kantor x) {
        for (int i = 0; i < x.getPegawai().length; i++) {
            if (x.getPegawai()[i] instanceof Marketing) {
                Marketing mar = (Marketing) x.getPegawai()[i];
                System.out.printf("%-5s", i + 1);
                System.out.printf("%-15s", mar.getNip());
                System.out.printf("%-10s", mar.getNama());
                if (mar.isStatus()) {
                    System.out.printf("%-10s", "Selesai");

                } else {
                    System.out.printf("%-10s", "studi");
                }
                System.out.printf("%-15s", mar.hitungGatot());
                System.out.println("");
            } else if (x.getPegawai()[i] instanceof Honorer) {
                Honorer hon = (Honorer) x.getPegawai()[i];
                System.out.printf("%-5s", i + 1);
                System.out.printf("%-15s", hon.getNip());
                System.out.printf("%-10s", hon.getNama());
                System.out.printf("%-10s", "-");
                System.out.printf("%-15s", hon.hitungGatot());
                System.out.println("");
            }

        }
    }

    static String statusm(Marketing x) {
        if (x.isSelesai()) {
            return "Selesai";
        }
        return "Studi";
    }

    public static void main(String[] args) {
        System.out.println("Struktur Kantor");
        System.out.println("");
        System.out.println("Data Manager");
        rekamPegawai();
        System.out.print("Jumlah Anak\t\t: ");
        jumAnak = baca.nextInt();
        System.out.print("Jam Kerja\t\t: ");
        jamKerja = baca.nextInt();
        do {
            System.out.print("Status (studi/selesai)\t: ");
            statusS = baca.next();
            if (null == statusS) {
                System.out.println("Masukan dengan benar");
            } else {
                switch (statusS) {
                    case "selesai":
                        status = true;
                        putar = false;
                        break;
                    case "studi":
                        status = false;
                        putar = false;
                        break;
                    default:
                        System.out.println("Masukan dengan benar");
                        System.out.println("");
                        break;
                }
            }

        } while (putar);
        Manager manager = new Manager(nama, nip, golongan, jumAnak, jamKerja, istri, status);
        kantor.setManager(manager);
        System.out.println("");
        System.out.print("Jumlah Pegawai\t\t: ");
        jumlah = baca.nextInt();
        System.out.print("Jumlah Honorer\t\t: ");
        anggotaH = baca.nextInt();
        anggotaM = jumlah - anggotaH;
        System.out.println("Jumlah Marketing\t: " + anggotaM);
        Pegawai[] pegawai = new Pegawai[jumlah];
        System.out.println("");
        if (anggotaH != 0) {
            for (int i = 0; i < anggotaH; i++) {
                System.out.println("Honorer ke-" + (i + 1));
                rekamPegawai();
                Honorer honor = new Honorer(nama, nip, golongan);
                pegawai[i] = honor;
                System.out.println("");
            }

        } else {
            System.out.println("");
        }
        if (anggotaM != 0) {
            for (int i = 0; anggotaH < pegawai.length; i++, anggotaH++) {
                System.out.println("Marketing ke-" + (i + 1));
                rekamPegawai();
                System.out.print("Jam Kerja\t\t: ");
                jamKerja = baca.nextInt();
                do {
                    System.out.print("Status (studi/selesai)\t: ");
                    statusS = baca.next();
                    if (null == statusS) {
                        System.out.println("Masukan dengan benar");
                    } else {
                        switch (statusS) {
                            case "selesai":
                                status = true;
                                putar = false;
                                break;
                            case "studi":
                                status = false;
                                putar = false;
                                break;
                            default:
                                System.out.println("Masukan dengan benar");
                                System.out.println("");
                                break;
                        }
                    }

                } while (putar);
                Marketing market = new Marketing(nama, nip, golongan, jamKerja, status);
                pegawai[i] = market;
            }
        }
        System.out.println("");
        
        kantor.setPegawai(pegawai);

        System.out.println("Nama Manager\t: " + kantor.getManager().nama);
        System.out.println("Daftar Pegawai");
        System.out.println("=======================================================");
        System.out.printf("%-5s", "No.");
        System.out.printf("%-15s", "NIP");
        System.out.printf("%-10s", "Nama");
        System.out.printf("%-10s", "Studi");
        System.out.printf("%-15s", "Gaji");
        System.out.println("");
        System.out.println("-------------------------------------------------------");
        cetak(kantor);
        System.out.println("=======================================================");
        System.out.println("Total Gaji: Rp."+kantor.hitGajipeg());
    }
}
