package m2tambahan_185314116;

public class Kantor {

    private Manager manager;
    private Pegawai[] pegawai;

    public Kantor() {
    }

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    public Pegawai[] getPegawai() {
        return pegawai;
    }

    public void setPegawai(Pegawai[] pegawai) {
        this.pegawai = pegawai;
    }

    public double hitGajipeg() {
        double totalGajiPeg = 0;
        for (int i = 0; i < this.pegawai.length; i++) {
            if (pegawai[i] instanceof Marketing) {
                Marketing m1 = (Marketing) pegawai[i];
                totalGajiPeg = m1.hitungGatot() + totalGajiPeg;
            } else {
                Honorer h = (Honorer) pegawai[i];
                totalGajiPeg = h.hitungGatot() + totalGajiPeg;
            }

        }
        return totalGajiPeg+manager.hitungGatot() ;
    }
}
