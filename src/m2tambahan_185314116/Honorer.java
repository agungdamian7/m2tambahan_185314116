package m2tambahan_185314116;

public class Honorer extends Pegawai {

    public Honorer() {
    }

    public Honorer(String nama, String nip, int golongan) {
        this.nama = nama;
        this.nip = nip;
        this.golongan = golongan;
    }

    @Override
    public double hitungGatot() {

        return hitGajiPokok();
    }

}
